// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var x int
	fmt.Scan(&x)
	if x < 50 {
		fmt.Println(x + 50)
	} else {
		fmt.Println(100)
	}
}
