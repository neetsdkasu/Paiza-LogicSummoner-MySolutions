// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

type Point struct {
	X, Y int
}

func check(m [][]int, n int) bool {
	sum := 0
	for _, x := range m[0] {
		sum += x
	}
	for _, xs := range m {
		tmp := 0
		for _, x := range xs {
			tmp += x
		}
		if tmp != sum {
			return false
		}
	}
	for j := 0; j < n; j++ {
		tmp := 0
		for i := 0; i < n; i++ {
			tmp += m[i][j]
		}
		if tmp != sum {
			return false
		}
	}
	tmp1 := 0
	tmp2 := 0
	for i := 0; i < n; i++ {
		tmp1 += m[i][i]
		tmp2 += m[i][n-1-i]
	}
	return tmp1 == sum && tmp2 == sum
}

func puts(m [][]int) {
	for _, xs := range m {
		for i, x := range xs {
			if i > 0 {
				fmt.Print(" ")
			}
			fmt.Print(x)
		}
		fmt.Println()
	}
}

func main() {
	var n int
	fmt.Scan(&n)
	m := make([][]int, n)
	flag := make([]bool, n*n+1)
	points := make([]Point, 0, 2)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			var t int
			fmt.Scan(&t)
			m[i] = append(m[i], t)
			if t == 0 {
				points = append(points, Point{j, i})
			}
			flag[t] = true
		}
	}
	unknowns := make([]int, 0, 2)
	for i, f := range flag {
		if !f {
			unknowns = append(unknowns, i)
		}
	}
	p0 := points[0]
	p1 := points[1]
	m[p0.Y][p0.X] = unknowns[0]
	m[p1.Y][p1.X] = unknowns[1]
	if !check(m, n) {
		m[p0.Y][p0.X] = unknowns[1]
		m[p1.Y][p1.X] = unknowns[0]
	}
	puts(m)
}
