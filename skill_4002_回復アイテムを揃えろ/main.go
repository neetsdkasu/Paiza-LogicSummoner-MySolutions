// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var m, n int
	fmt.Scan(&m, &n)
	total := m * n
	fmt.Println(total)
}
