// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	_ "math/rand"
	_ "time"
)

const (
	E = iota
	R
	G
	B
)

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func Max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

type Pos struct{ X, Y int }

func (p Pos) String() string {
	return fmt.Sprintf("%d %d", p.X, p.Y)
}

type Problem struct {
	W, H, N int
	Field   [][]int
}

func NewProblem(w, h, n int) (p *Problem) {
	p = new(Problem)
	p.W, p.H, p.N = w, h, n
	p.Field = make([][]int, w)
	for x := range p.Field {
		p.Field[x] = make([]int, h)
	}
	return
}

func (src *Problem) CopyTo(dst *Problem) {
	for x := range dst.Field {
		copy(dst.Field[x], src.Field[x])
	}
}

func (p *Problem) dropBlocks(minX, maxX int) {
	for _, vert := range p.Field[minX : maxX+1] {
		emp := -1
		for y, color := range vert {
			switch {
			case emp >= 0 && color != E:
				vert[emp] = color
				vert[y] = E
				emp++
			case emp < 0 && color == E:
				emp = y
			}
		}
	}
	return
}

func (p *Problem) isInField(x, y int) bool {
	return 0 <= x && x < p.W && 0 <= y && y < p.H
}

var delta = []Pos{{1, 0}, {0, 1}, {-1, 0}, {0, -1}}

func (p *Problem) eraseBlocks(base Pos) (count, minX, maxX int) {
	minX, maxX = base.X, base.X
	color := p.Field[base.X][base.Y]
	if color == E {
		println("NO BLOCK")
		return
	}
	count = 1
	p.Field[base.X][base.Y] = E
	targets := []Pos{base}
	for len(targets) > 0 {
		last := len(targets) - 1
		pos := targets[last]
		targets = targets[:last]
		for _, dt := range delta {
			x, y := pos.X+dt.X, pos.Y+dt.Y
			if p.isInField(x, y) && p.Field[x][y] == color {
				count++
				p.Field[x][y] = E
				targets = append(targets, Pos{x, y})
				minX = Min(minX, x)
				maxX = Max(maxX, x)
			}
		}
	}
	return
}

func (p *Problem) labeling() (labelPos []Pos, labelCounts []int) {
	labelPos = make([]Pos, 0, p.W*p.H)
	labelCounts = make([]int, 0, p.W*p.H)
	labeledField := make([][]int, p.W)
	for x := range labeledField {
		labeledField[x] = make([]int, p.H)
	}
	targets := make([]Pos, 0, p.W*p.H)
	labelID := 0
	for x, vert := range p.Field {
		for y, color := range vert {
			if color == E || labeledField[x][y] > 0 {
				continue
			}
			pos := Pos{x, y}
			labelID++
			labeledField[x][y] = labelID
			targets = append(targets[:0], pos)
			count := 1
			for len(targets) > 0 {
				size := len(targets) - 1
				tag := targets[size]
				targets = targets[:size]
				for _, dt := range delta {
					tx, ty := tag.X+dt.X, tag.Y+dt.Y
					if !p.isInField(tx, ty) {
						continue
					}
					if p.Field[tx][ty] != color {
						continue
					}
					if labeledField[tx][ty] > 0 {
						continue
					}
					count++
					labeledField[tx][ty] = labelID
					targets = append(targets, Pos{tx, ty})
				}
			}
			if count > 1 {
				labelPos = append(labelPos, pos)
				labelCounts = append(labelCounts, count)
			}
		}
	}
	if len(labelPos) > 10 {
		return
	}
	for x, vert := range p.Field {
		if vert[0] == E {
			continue
		}
		labelPos = append(labelPos, Pos{x, 0})
		labelCounts = append(labelCounts, 1)
	}
	return
}

func (p *Problem) Solve() (ans []Pos) {
	temp2 := NewProblem(p.W, p.H, p.N)
	temp3 := NewProblem(p.W, p.H, p.N)
	allBlockCount := p.W * p.H
	for k := 0; k < p.N; k++ {
		maxCount := 0
		sel := 0
		labelPos, _ := p.labeling()
		for i, pos := range labelPos {
			p.CopyTo(temp2)
			count1, minX, maxX := temp2.eraseBlocks(pos)
			temp2.dropBlocks(minX, maxX)
			countZ := 0
			if k+1 < p.N {
				temp2LabelPos, temp2LabelCounts := temp2.labeling()
				if p.N/2 < k && k+2 < p.N {
					size := Min(10, len(temp2LabelPos))
					for _, temp2Pos := range temp2LabelPos[:size] {
						temp2.CopyTo(temp3)
						count2, minX, maxX := temp3.eraseBlocks(temp2Pos)
						temp3.dropBlocks(minX, maxX)
						_, temp3LabelCounts := temp3.labeling()
						for _, count3 := range temp3LabelCounts {
							countZ = Max(countZ, count2+count3)
						}
					}
				} else {
					for _, count2 := range temp2LabelCounts {
						countZ = Max(countZ, count2)
					}
				}
			}
			if count1+countZ > maxCount {
				maxCount = count1 + countZ
				sel = i
			}
		}
		pos := labelPos[sel]
		ans = append(ans, pos)
		count, minX, maxX := p.eraseBlocks(pos)
		p.dropBlocks(minX, maxX)
		allBlockCount -= count
		if allBlockCount == 0 {
			break
		}
	}
	return
}

func main() {
	var w, h, n int
	fmt.Scan(&w, &h, &n)
	p := NewProblem(w, h, n)
	for i := 1; i <= p.H; i++ {
		var s string
		fmt.Scan(&s)
		for j, ch := range s {
			var color int
			switch ch {
			case 'R':
				color = R
			case 'G':
				color = G
			case 'B':
				color = B
			default:
				panic("INVALID INPUT")
			}
			p.Field[j][p.H-i] = color
		}
	}
	ans := p.Solve()
	fmt.Println(len(ans))
	for _, pos := range ans {
		fmt.Println(pos.X+1, p.H-pos.Y)
	}
}
