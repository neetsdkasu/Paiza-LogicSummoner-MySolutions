// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var s string
	var t int
	fmt.Scan(&s, &t)
	for _, r := range s {
		if r == '1' {
			t--
		}
	}
	if t > 0 {
		fmt.Println(t)
	} else {
		fmt.Println("No")
	}
}
