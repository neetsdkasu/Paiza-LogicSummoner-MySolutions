// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var s string
	fmt.Scan(&s)
	rs := ([]rune)(s)
	xs := make([]int, 0, 100)
	if rs[0] == 'w' {
		xs = append(xs, 0)
	}
	cnt := 0
	ch := rs[0]
	for _, r := range rs {
		if ch == r {
			cnt++
		} else {
			xs = append(xs, cnt)
			cnt = 1
			ch = r
		}
	}
	if cnt > 0 {
		xs = append(xs, cnt)
	}
	for i, c := range xs {
		if i > 0 {
			fmt.Print(" ")
		}
		fmt.Print(c)
	}
	fmt.Println()
}
