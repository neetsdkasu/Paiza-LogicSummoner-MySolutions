// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	damages := 0
	for i := 0; i < 5; i++ {
		var cmd string
		fmt.Scan(&cmd)
		if cmd == "Attack" {
			damages += 100
		}
	}
	fmt.Println(damages)
}
