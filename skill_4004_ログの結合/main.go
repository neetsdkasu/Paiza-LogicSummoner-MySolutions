// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var n int
	fmt.Scan(&n)
	text := make([]string, n)
	flag := make([]bool, n)
	for i := 0; i < n; i++ {
		var d int
		var w string
		fmt.Scan(&d, &w)
		flag[d] = true
		text[d] += w
	}
	for i, f := range flag {
		if !f {
			continue
		}
		fmt.Printf("%d %s\n", i, text[i])
	}
}
