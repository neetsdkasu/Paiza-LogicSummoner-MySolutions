// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var n, m, a, b int
	fmt.Scan(&n, &m, &a, &b)
	for i := 0; i < n; i++ {
		var d int
		fmt.Scan(&d)
		m += d
		if d < 0 && m <= a {
			m += b
		}
	}
	fmt.Println(m)
}
