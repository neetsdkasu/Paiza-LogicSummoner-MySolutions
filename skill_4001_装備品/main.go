// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var a, b, c, d int
	fmt.Scan(&a, &b, &c, &d)
	if a*d > c*b {
		fmt.Println(b)
	} else {
		fmt.Println(d)
	}
}
