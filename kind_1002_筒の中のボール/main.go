// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var n int
	var s string
	fmt.Scan(&n, &s)
	flag := false
	f := func(ch uint8, b, e, d int) {
		for i := b; i != e; i += d {
			if s[i-1] != ch {
				continue
			}
			if flag {
				fmt.Print(" ")
			} else {
				flag = true
			}
			fmt.Print(i)
		}
	}
	f('L', len(s), 0, -1)
	f('R', 1, len(s)+1, 1)
	fmt.Println()
}
