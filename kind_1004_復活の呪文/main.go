// Try paiza Logic Summoner
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
)

func main() {
	var a, b string
	fmt.Scan(&a, &b)
	if a == b {
		fmt.Println("OK")
	} else {
		fmt.Println("NG")
	}
}
